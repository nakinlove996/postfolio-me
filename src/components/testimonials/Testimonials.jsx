import './testimonials.scss'

export default function Testimonals() {

    const data = [
        {
            id: 1,
            name: "Ford Durden",
            title: "Senior Developer",
            img:
                "assets/11.jpeg",
            icon: "assets/twitter.png",
            desc:
                "Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat magnam dolorem.",
        },
        {
            id: 2,
            name: "Nakin Boonsingma",
            title: "Co-Founder of DELKA",
            img:
                "assets/11.jpeg",
            icon: "assets/youtube.png",
            desc:
                "Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat magnam dolorem recusandae perspiciatis ducimus vel hic temporibus. ",
            featured: true,
        },
        {
            id: 3,
            name: "Ford Harold",
            title: "CEO of ALBI",
            img:
                "assets/11.jpeg",
            icon: "assets/linkedin.png",
            desc:
                "Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat magnam dolorem",
        },
    ];

    return (
        <div className="testimonials" id="testimonials">
            <h1>testimonials</h1>
            <div className="container">
                {data.map((d) => (

                    <div className={d.featured ? "card featured" : "card"}>
                        <div className="top">
                            <img src="assets/right-arrow.png" className="left" alt="" />
                            <img src={d.img}className="user" alt="" />
                            <img className="right" src={d.icon} alt="" />
                        </div>
                        <div className="center">
                            {d.desc}
                        </div>
                        <div className="bottom">
                            <h3>{d.name}</h3>
                            <h4>{d.title}</h4>
                        </div>
                    </div>

                ))}

            </div>
        </div>
    )
}
